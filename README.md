MyPlaces is a Cordova app assignment for Open University of Catalonia (Universitat Oberta de Catalunya - UOC).

It is about implementing a hybrid mobile app that will let users:

* Login or register in the system.
* Manage a list of favorite places by adding description, picture, and GPS coordinates.
* Rate other user's places.

It was developed with Cordova framework (https://cordova.apache.org/) and Eclipse + Aptana plugin (http://www.aptana.com/products/studio3/success_plugin.html).

Also, Parse (http://parse.com/) is used as backend for user credentials, places descriptions, pictures and coordinates.