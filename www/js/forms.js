/**
 * 
 * Functions to clear and validate forms. 
 * 
 */

/**
 * clearLogInForm clears Log In form and disables Log In button.
 */
function clearLogInForm() {
	
	$("#login_form").clearForm();
	$("#login_button").button("disable");
}

/**
 * clearRegisterForm clears Register form and disables Register button.
 */
function clearRegisterForm() {
	
	$("#register_form").clearForm();
	$("#register_button").button("disable");
}

/**
 * clearEditForm clears Edit form.
 */
function clearEditForm() {
	
	$("#edit_form").clearForm();
	$("#edit_place_ok_button").addClass("ui-disabled");
}

/**
 * loginFormCheck decides if Login button has to be enabled or disabled.
 * 
 * TODO Refactor (2)
 */
function loginFormCheck() {
	
	// Gather values in form elements:
	var a = $("#login_username_input").val() != '';
	var b = $("#login_password_input").val() != '';
	
	// All values OK means that the button can be enabled:
	if (a && b) {
		
		$("#login_button").button("enable");
	}
	
	else {
		
		$("#login_button").button("disable");
	}
}

/**
 * registerFormCheck decides if Register button has to be enabled or disabled.
 * 
 * TODO Refactor (2)
 */
function registerFormCheck() {
	
	// Gather values in form elements:
	var a = $("#register_name_input").val() != '';
	var b = $("#register_username_input").val() != '';
	var c = $("#register_password_input").val() != '';
	var d = $("#register_repeat_password_input").val() != '';
	var e = $("#register_email_input").val() != '';
	var f = $("#register_terms_input").prop('checked');
	
	// All values OK means that the button can be enabled:
	if (a && b && c && d && e && f) {
		
		$("#register_button").button("enable");
	}
	
	else {
		
		$("#register_button").button("disable");
	}
}

/**
 * editFormCheck decides if OK button in Edit page has to be enabled or disabled.
 * 
 * TODO Refactor (2)
 */
function editFormCheck() {
	
	// Gather values in form elements:
	var a = $("#edit_name_input").val() != '';
	var b = $("#edit_description_input").val() != '';
	var c = $("#edit_latitude_input").val() != '';
	var d = $("#edit_longitude_input").val() != '';
	
	// All values OK means that the button can be enabled:
	if (a && b && c && d) {
		
		$("#edit_place_ok_button").removeClass("ui-disabled");
	}
	
	else {
		
		$("#edit_place_ok_button").addClass("ui-disabled");
	}
}

