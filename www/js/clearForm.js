/**
 * jQuery plugin to clear a form.
 * 
 * http://www.learningjquery.com/2007/08/clearing-form-data
 * 
 * type == 'email' support added.
 * type == 'number' support added.
 */

$.fn.clearForm = function() {
	
	return this.each(function() {
		
		var type = this.type;
		var tag = this.tagName.toLowerCase();
		
		if (tag == 'form') {
			
			return $(':input',this).clearForm();
		}
		
		if (type == 'text' || type == 'password' || type == 'email' || type == 'number' || tag == 'textarea') {
			
			this.value = '';
		}
		
		else if (type == 'checkbox' || type == 'radio') {
			
			this.checked = false;
		}
		
		else if (tag == 'select') {
			
			this.selectedIndex = -1;
		}
		
		/*else if (tag == 'img') {
			
			this.src = '';
		}*/
  });
};
