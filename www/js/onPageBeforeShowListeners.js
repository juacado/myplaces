/**
 * 
 * onPageBeforeShow event listeners for pages.
 * 
 */

/**
 * onLogInPageBeforeShow is triggered just before Log In page is going to be shown.
 * 
 * @param {Object} event
 */
function onLogInPageBeforeShow(event) {
	
	clearLogInForm();
}

/**
 * onRegisterPageBeforeShow is triggered just before Register page is going to be shown.
 * 
 * @param {Object} event
 */
function onRegisterPageBeforeShow(event) {
	
	clearRegisterForm();
}

/**
 * onMyPlacesPageBeforeShow is triggered just before My Places page is going to be shown.
 * 
 * @param {Object} event
 */
function onMyPlacesPageBeforeShow(event) {
	
	clearList();
	fillMyPlacesPage();
}

/**
 * onEditPlacePageBeforeShow is triggered just before Edit Place page is going to be shown.
 * 
 * @param {Object} event
 */
function onEditPlacePageBeforeShow(event) {
	
	//alert(currentPlace);
	//alert(currentPhoto);
	//if ((currentPlace == null) && (currentPhoto == null)) {
	if (currentPlace) {
		
		clearEditForm();
	};
	fillEditCurrentPosition();
	/*NEW*/
	//if (currentPlace) fillEditPage();
	fillEditPage();
}

/*NEW*/
function fillEditCurrentPosition() {
	
	// TODO Refactor (5)
	var latitude = currentPosition.lat();
	var longitude = currentPosition.lng();
		
	$("#edit_latitude").text(latitude);
	$("#edit_longitude").text(longitude);
}

/**
 * onViewPlacePageBeforeShow is triggered just before View Place page is going to be shown.
 * 
 * @param {Object} event
 */
function onViewPlacePageBeforeShow(event) {
	
	fillViewPage();
}

/**
 * onMyProfilePageBeforeShow is triggered just before My Profile page is going to be shown.
 * 
 * @param {Object} event
 */
function onMyProfilePageBeforeShow(event) {
	
	fillUserProfilePage();
}

function onMapToSelectCoordinatesPageBeforeShow(event) {
	
	initializeGoogleMaps();
}
