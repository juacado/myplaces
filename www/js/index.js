/**
 * The event "deviceready" is triggered when Cordova is fully loaded.
 * 
 * From this point we can safely make calls to Cordova APIs, so we define a listener for that event.
 * 
 * https://cordova.apache.org/docs/en/4.0.0/cordova_events_events.md.html#deviceready
 */
document.addEventListener("deviceready", onDeviceReady, false);

/**
 * Constant that holds the app name. Mainly used in messages.
 */
var APP_NAME = "MyPlaces";

/**
 * Places in this app extends Parse Object.
 */
var Place = Parse.Object.extend("Place");

/**
 * Default values when app is launched.
 * 
 * currentPlace is null because there is no place selected or being created yet.
 * currentPhoto is null because there is no place selected or being created yet and we don't have any photo. 
 * currentPosition will be overridden with actual position if navigator.geolocation is supported.
 */
var currentPlace = null;
//var currentPhoto = null;
var currentPosition = new google.maps.LatLng(41.380860, 2.122832); // By default is FCB Camp Nou Stadium :)

/**
 * Flag to indicate whether the List View to display user's places is created or not.
 */
var placesListCreated = false;
 
var placeToViewId = null;
//var base64Image = null;

/**
 * We need to know what we are doing in Edit page: creating a new place or editing a place that already exists. 
 */
var NEW_MODE = 0;
var EDIT_MODE = 1;
var MODE = null;

/**
 * onDeviceReady is the listener for event "deviceready".
 */
function onDeviceReady() {
	
	/**
	 * We try to get current position to provide more realistic info to the user:
	 */	
	if (navigator.geolocation) { getCurrentPosition(); };
    	
	/**
	 * Initialize Parse and perform a small test to make sure it is working properly.
	 */
	var APPLICATION_ID = "Dngvuh7DSZuK3j9sLBT8p5YQkCK2IV0emSJn9Wbf";
	var JAVASCRIPT_KEY = "J2TVIduGCiAzKGk8gNzScE1DvpKwc6zI9AogYEJv";
	Parse.initialize(APPLICATION_ID, JAVASCRIPT_KEY);
    
    var TestObject = Parse.Object.extend("TestObject");
    var testObject = new TestObject();
    testObject.save({ foo: "bar" }, { success: onParseInitializedSuccess, error: onParseInitializedError });
    
	/**
	 * We define listeners for the event onClick for each button in the UI that needs it.
	 */
	$("#login_button").click(onLogInButtonClick);
	$("#register_button").click(onRegisterButtonClick);
	$("#new_place_button").click(onNewPlaceButtonClick);
	//$("#edit_place_cancel_button").click(onEditPlaceCancelButtonClick);
	$("#edit_place_ok_button").click(onEditPlaceOKButtonClick);
	$("#edit_place_button").click(onEditPlaceButtonClick);
	$("#logout_button").click(onLogOutButtonClick);
	$("#select_coordinates_from_map_button").click(onSelectCoordinatesFromMapButtonClick);
	
	/*NEW*///$("#map_ok_button").click(onMapOKButtonClick);
	
	$("#take_photo_button").click(onTakePhotoButtonClick);
	
	/**
	 * In order to enable/disable Log In button when all elements in Log In form are correct,
	 * we define the function that decides that as listener for the event:
	 * - onKeyUp in input texts.
	 */
	$("#login_username_input").keyup(loginFormCheck);
	$("#login_password_input").keyup(loginFormCheck);
	
	/**
	 * In order to enable/disable Register button when all elements in Register form are correct,
	 * we define the function that decides that as listener for the events:
	 * - onKeyUp in input texts,
	 * - onChange in Terms and Conditions checkbox.
	 */
	$("#register_name_input").keyup(registerFormCheck);
	$("#register_username_input").keyup(registerFormCheck);
	$("#register_password_input").keyup(registerFormCheck);
	$("#register_repeat_password_input").keyup(registerFormCheck);
	$("#register_email_input").keyup(registerFormCheck);
	$("#register_terms_input").change(registerFormCheck);
	
	/**
	 * In order to enable/disable OK button when all elements in Edit form are correct,
	 * we define the function that decides that as listener for the events:
	 * - onKeyUp in input texts.
	 */
	$("#edit_name_input").keyup(editFormCheck);
	$("#edit_description_input").keyup(editFormCheck);
	// TODO How to know if there is a photo?
	
	/**
	 * We define listeners for the event onPageBeforeShow for each page that needs it.
	 */	
	$("#login_page").on("pagebeforeshow", onLogInPageBeforeShow);
	$("#register_page").on("pagebeforeshow", onRegisterPageBeforeShow);
	$("#my_places_page").on("pagebeforeshow", onMyPlacesPageBeforeShow);
	$("#edit_place_page").on("pagebeforeshow", onEditPlacePageBeforeShow);
	$("#view_place_page").on("pagebeforeshow", onViewPlacePageBeforeShow);
	$("#my_profile_page").on("pagebeforeshow", onMyProfilePageBeforeShow);
	$("#map_to_select_coordinates").on("pagebeforeshow", onMapToSelectCoordinatesPageBeforeShow);
}

/**
 * Sets the mode in which the app is working. 
 * 
 * @param {Object} mode NEW or EDIT
 */
function setMode(mode) {
	
	MODE = mode;
}

function getCurrentPosition() {
	
	navigator.geolocation.getCurrentPosition(getCurrentPositionSuccess, getCurrentPositionError);
}

function getCurrentPositionSuccess(position) {
	
	currentPosition = position;
}

function getCurrentPositionError(error) {
}
