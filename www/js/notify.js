/**
 * Notifies the user with a message.
 * 
 * This function tries first to use notification plugin, if it doesn't exists
 * then message is shown via alert browser.
 *  
 * @param {Object} title
 * @param {Object} message
 */

function notify(title, message) {
	
	if (navigator.notification) {
		
		console.log('navigator');
		
		navigator.notification.alert(message, null, title, 'OK');
	}
	
	else {
		
		window.alert(mensaje);
	}
}
