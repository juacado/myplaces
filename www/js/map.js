var map;
var mapMarker;

function createGoogleMaps(center) {

    var mapCanvas = document.getElementById('map_to_select_coordinates_canvas');
    var mapOptions = {
        
        center: center,
        zoom: 8,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    
    return new google.maps.Map(mapCanvas, mapOptions);
}

function putMarker(map, position, title) {
	
    var markerOptions = {
        
        position: position,
        map: map,
        title: title
    };
    
    return new google.maps.Marker(markerOptions);
}

function onMapClick(event) {
    
    currentPosition = event.latLng;
    mapMarker.setPosition(currentPosition);
    
    // TODO Refactor (5)
    var latitude = currentPosition.lat();
    var longitude = currentPosition.lng();
    $("#map_latitude").text(latitude);
    $("#map_longitude").text(longitude);
}

function initializeGoogleMaps() {
    
    var position = currentPosition;
    map = createGoogleMaps(position);
    mapMarker = putMarker(map, position, 'Test Location');
    
    google.maps.event.addListener(map, 'click', onMapClick);
    
    // TODO Refactor (5)
    var latitude = position.lat();
	var longitude = position.lng();
	$("#map_latitude").text(latitude);
	$("#map_longitude").text(longitude);
}