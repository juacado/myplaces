/**
 * 
 * Functions related to ListView that holds places that the user has created. 
 * 
 */

/**
 * createListView creates an Unordered List to hold user's places.
 */
function createListView() {
    
    $("#places_list_content").append("<ul id='places_list' data-role='listview' data-inset='true'></ul>");
    $("#places_list_content").trigger("create");
}

/**
 * clearList empties the list that holds user's places.
 */
function clearList() {
    
    $("#places_list_content").empty();
    placesListCreated = false;
}

/**
 * appendToList adds a place to the end of the list that holds user's places.
 * 
 * @param {Object} name
 * @param {Object} description
 */
function appendToList(id, name, description){
    
    if (!placesListCreated) {
        
        createListView();
        placesListCreated = true;
    }
    
    $("#places_list").append("<li><a id='" + id + "' href='#'>" + name + "</br>" + description + "</a></li>");
    $("#places_list").listview("refresh");
    
    $('#' + id).click({ id: id }, onPlaceClick);
}
