/**
 * 
 * Parse callbacks.
 *  
 */

/**
 * onParseInitializedSuccess is triggered when Parse is initialized succesfully.
 * 
 * @param {Object} object
 */
function onParseInitializedSuccess(object) {
}

/**
 * onParseInitializedError is triggered when there is an error initializing Parse.
 * 
 * @param {Object} model
 * @param {Object} error
 */
function onParseInitializedError(model, error) {
	
	switch (error.code) {
		
		default:
			
			notify(APP_NAME, MESSAGE_PARSE_INITIALIZED_ERROR);
	}
}

/**
 * onUserLogInSuccess is triggered when user is logged in successfully.
 * 
 * @param {Object} user
 */
function onUserLogInSuccess(user) {
	
	$.mobile.changePage("#my_places_page");
}

/**
 * onUserLogInError is triggered when there is an error in user log in process.
 * 
 * @param {Object} user
 * @param {Object} error
 */
function onUserLogInError(user, error) {
	
	switch (error.code) {
		
		case 101:
			
			notify(APP_NAME, MESSAGE_INVALID_LOGIN_PARAMETERS);
			break;
			
		default:
			
			notify(APP_NAME, MESSAGE_UNEXPECTED_ERROR + "onUserLogInError: (" + error.code + ") " + error.message);
	}
}

/**
 * onUserSignUpSuccess is triggered when user sign up process is completed successfully.
 * 
 * @param {Object} user
 */
function onUserSignUpSuccess(user) {
	
	notify(APP_NAME, MESSAGE_SUCCESFULLY_SIGNED_UP);
	
	$.mobile.changePage("#my_places_page");
}

/**
 * onUserSignUpError is triggered when there is an error in user sign up process.
 * 
 * @param {Object} user
 * @param {Object} error
 */
function onUserSignUpError(user, error) {
	
	switch (error.code) {
		
		case 125:
			
			notify(APP_NAME, MESSAGE_INVALID_EMAIL_ADDRESS_FORMAT);
			break;
			
		default:
		
			notify(APP_NAME, MESSAGE_UNEXPECTED_ERROR + "onUserSignUpError: (" + error.code + ") " + error.message);
	}
}

/**
 * onFindMyPlacesSuccess is triggered when query to find places that the user has created is completed successfully.
 * 
 * @param {Object} results
 */
function onFindMyPlacesSuccess(results) {
	
	for (var i = 0; i < results.length; i++) {
		
		var place = results[i];
		
		var id = place.id;
		var name = place.get("name");
		var description = place.get("description");
		
		appendToList(id, name, description);
	}
}

/**
 * onFindMyPlacesError is triggered when there is an error in query to find places that the user has created.
 * 
 * @param {Object} user
 * @param {Object} error
 */
function onFindMyPlacesError(error) {
	
	switch (error.code) {
		
		default:
			
			notify(APP_NAME, MESSAGE_PARSE_INITIALIZED_ERROR);
	}
}

/**
 * onTakePhotoSuccess is triggered when a photo is taken successfully and the user accepts.
 * 
 * @param {Object} imageURI
 */
function onTakePhotoSuccess(imageData) {
	
	// TODO Refactor (4)
	//base64Image = imageData;
	//alert(JSON.stringify(imageData));
    $("#edit_photo").attr("src", "data:image/jpeg;base64," + imageData);
}

/**
 * onTakePhotoError is triggered when the user cancels taking a photo.
 * 
 * @param {Object} message
 */
function onTakePhotoError(message) {
}

function onSavePhotoPlaceSuccess(photo) {
}

function onSavePhotoPlaceError(error){
	
	switch (error.code) {
		
		default:
		
			notify(APP_NAME, MESSAGE_UNEXPECTED_ERROR + "onSavePhotoPlaceError: (" + error.code + ") " + error.message);
	}
}

/**
 * onSavePlaceSuccess is triggered when place is saved successfully.
 * 
 * @param {Object} place
 */
function onSavePlaceSuccess(place) {
	
	currentPlace = place;
	placeToViewId = currentPlace.id;
	$.mobile.changePage("#view_place_page");
}

/**
 * onSavePlaceError is triggered when there is an error when saving a place.
 * 
 * @param {Object} place
 * @param {Object} error
 */
function onSavePlaceError(place, error) {
		
	switch (error.code) {
		
		case 107:
		
			notify(APP_NAME, MESSAGE_INVALID_LOCATION_FORMAT);
			break;
		
		default:
		
			notify(APP_NAME, MESSAGE_UNEXPECTED_ERROR + "onSavePlaceError: (" + error.code + ") " + error.message);
	}
}

function onGetPlaceSuccess(place) {
	
	currentPlace = place;
	
	var name = place.get("name");
	var description = place.get("description");
	var location = place.get("location");
	var latitude = location.latitude;
	var longitude = location.longitude;
	var photo = place.get("photo");
	
	$("#view_name").text(name);
	$("#view_description").text(description);
	
	// TODO Refactor (5)
	$("#view_latitude").text(latitude);
	$("#view_longitude").text(longitude);
	
	$("#view_photo").attr("src", photo.url());
}

function onGetPlaceError(place, error) {
		
	switch (error.code) {
		
		case 101:
		
			notify(APP_NAME, MESSAGE_QUERY_OBJECT_NOT_FOUND);
			break;
		
		default:
		
			notify(APP_NAME, MESSAGE_UNEXPECTED_ERROR + "onGetPlaceError: (" + error.code + ") " + error.message);
	}
}
