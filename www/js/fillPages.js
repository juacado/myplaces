/**
 * 
 */
function fillMyPlacesPage() {
	
	var currentUser = Parse.User.current();
	var query = new Parse.Query(Place);
	
	query.select("name", "description");
	query.equalTo("user", currentUser);
	query.find({ success: onFindMyPlacesSuccess, error: onFindMyPlacesError });
}

/**
 * 
 */
function fillUserProfilePage() {
	
	// Get current user info.
	var currentUser = Parse.User.current();
	
	// Make sure there is a user logged in (otherwise something wrong is happening!)
	if (currentUser) {
		
		// Gather values from the current user:
		var name = currentUser.get("name");
		var username = currentUser.get("username");
		var email = currentUser.get("email");
		
		// Display those values in UI elements:
		$("#my_profile_name").text(name);
		$("#my_profile_username").text(username);
		$("#my_profile_email").text(email);
	}
}

/**
 * 
 */
function fillViewPage() {
	
	var query = new Parse.Query(Place);
	query.get(placeToViewId, { success: onGetPlaceSuccess, error: onGetPlaceError });
}

/**
 * 
 */
function fillEditPage() {
	
	var name = currentPlace.get("name");
	var description = currentPlace.get("description");
	var location = currentPlace.get("location");
	var photo = currentPlace.get("photo");
	
	$("#edit_name_input").val(name);
	$("#edit_description_input").val(description);
	$("#edit_photo").attr("src", photo.url());
}
