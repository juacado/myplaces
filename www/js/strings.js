/**
 * 
 * Strings to be used in alert messages.
 * 
 */
var MESSAGE_PARSE_INITIALIZED_ERROR = "Unexpected error. Check your Internet connection and relaunch the app.";
var MESSAGE_PASSWORD_TWICE = "Password and Repeat Password MUST be the same.";
var MESSAGE_INVALID_LOGIN_PARAMETERS = "Either username or password are incorrect.";
var MESSAGE_INVALID_EMAIL_ADDRESS_FORMAT = "Invalid email address format.";
var MESSAGE_INVALID_LOCATION_FORMAT = "Invalid location format.";
var MESSAGE_SUCCESFULLY_SIGNED_UP = "You are successfully signed up!";
var MESSAGE_SUCCESFULLY_PLACE_SAVED = "Your place was succesfully saved!";
var MESSAGE_QUERY_OBJECT_NOT_FOUND = "Your place cannot be found in the database.";
var MESSAGE_UNEXPECTED_ERROR = "Unexpected error. Please contact app developer.";
