/**
 * 
 * onClick event listeners for buttons.
 *  
 */

/**
 * onLogInButtonClick is triggered when Log In button is clicked in Log In page.
 */
function onLogInButtonClick() {
	
	// Gather values from the form:
	var username = $("#login_username_input").val();
	var password = $("#login_password_input").val();
	
	Parse.User.logIn(username, password, { success: onUserLogInSuccess, error: onUserLogInError });
}

/**
 * onRegisterButtonClick is triggered when Register button is clicked in Register page.
 */
function onRegisterButtonClick() {
	
	// Gather values from the form:
	var name = $("#register_name_input").val();
	var username = $("#register_username_input").val();
	var password = $("#register_password_input").val();
	var repeat_password = $("#register_repeat_password_input").val();
	var email = $("#register_email_input").val();
	
	// Perform validation:
	var valid = (password == repeat_password);
	
	if (valid) {
		
		// Create new user with gathered data and sign up:
		var user = new Parse.User();
		user.set("name", name);
		user.set("username", username);
		user.set("password", password);
		user.set("email", email);
		
		user.signUp(null, { success: onUserSignUpSuccess, error: onUserSignUpError });
	}
	
	else {
		
		notify(APP_NAME, MESSAGE_PASSWORD_TWICE);
	}	
}

/**
 * onNewPlaceButtonClick is triggered when + button is clicked in My Places page.
 */
function onNewPlaceButtonClick() {
	
	setMode(NEW_MODE);
	currentPlace = null;
}

/**
 * onEditOKButtonClick is triggered when OK button is clicked in Edit Place page.
 */
function onEditPlaceOKButtonClick() {
	
	// Get current user info.
	var currentUser = Parse.User.current();
	
	// Make sure there is a user logged in (otherwise something wrong is happening!)
	if (currentUser) {
		
		// Gather values from the form:
		var name = $("#edit_name_input").val();
		var description = $("#edit_description_input").val();
		
		var latitude = currentPosition.lat();
		var longitude = currentPosition.lng();
		
		var location = new Parse.GeoPoint({ latitude: /*parseInt(*/latitude/*)*/, longitude: /*parseInt(*/longitude/*)*/ });
		
		// Save photo before we can use it:
		var photo = new Parse.File(name + ".jpeg", { base64: base64Image });
		photo.save({ success: onSavePhotoPlaceSuccess, error: onSavePhotoPlaceError });
		
		var place = null;
		
		// Create a new Place or use the current one to update it:
		switch (MODE) {
			
			case NEW_MODE:
				
				place = new Place();
				break;
				
			case EDIT_MODE:
				
				place = currentPlace;
				break;
		}
		
		// Create new Place with gathered data and save it:
		place.set("user", currentUser);
		place.set("name", name);
		place.set("description", description);
		place.set("location", location);
		place.set("photo", photo);
		
		place.save(null, { success: onSavePlaceSuccess, error: onSavePlaceError });
	}
}

/**
 * onTakePhotoButtonClick is triggered when Take a Photo button is clicked in Edit Place page.
 */
function onTakePhotoButtonClick(event) {
	
	var cameraOptions = {
		
		destinationType: Camera.DestinationType.DATA_URL,
		targetWidth: 800,
		targetHeight: 600
	};
	navigator.camera.getPicture(onTakePhotoSuccess, onTakePhotoError, cameraOptions);
}

/**
 * onEditPlaceButtonClick is triggered when Edit button is clicked in View Place page.
 */
function onEditPlaceButtonClick() {
	
	setMode(EDIT_MODE);
}

/**
 * onLogOutButtonClick is triggered when Log Out button is clicked in My Profile page.
 */
function onLogOutButtonClick() {
	
	Parse.User.logOut();
}

/**
 * onPlaceClick is triggered when a place in List View is clicked in My Places page.
 */
function onPlaceClick(event){
	
	placeToViewId = event.data.id;
	$.mobile.changePage("#view_place_page");
}

function onSelectCoordinatesFromMapButtonClick() {
	
	$.mobile.changePage("#map_to_select_coordinates");
}

/*function onMapOKButtonClick() {
	
}*/
